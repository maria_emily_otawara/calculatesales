package jp.alhinc.otawara_emily.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	//javaコマンドのエントリーポイントとなる特別なメソッド「mainメソッド」を実行する
	 public static void main(String[] args) {
		 BufferedReader br = null;
		 BufferedReader breader = null;
		 Map<String, String> prefs = new HashMap<String, String>();
		 Map<String, Long> profit = new HashMap<String, Long>();

		 try {
			//コマンドライン引数が2以上の場合のエラー処理
			 if (args.length != 1) {
				 System.out.println("予期せぬエラーが発生しました");
				 return;
			 }

			 //支店リスト取得
			 File file = new File(args[0], "branch.lst");
			 //argsの中身が空の場合のエラー処理
			 if (args[0].equals(null)) {
				 System.out.println("予期せぬエラーが発生しました");
				 return;
			 }

			 //支店定義ファイルが存在しない場合のエラー処理
			 if (!file.exists()) {
				 System.out.println("支店定義ファイルが存在しません");
				 return;
			 }

			 //支店定義ファイルを開く
			 FileReader fr = new FileReader(file);
			 //支店定義ファイルを読み込む
			 br = new BufferedReader(fr);
			 String line;

			 //BufferedReaderクラスのreadLineメソッドを使って1行ずつ読み込み表示する
			 while((line = br.readLine()) != null) {
				 //splitメソッドでデータを分割し、それぞれを配列の要素として保持する
				 String[] branch = line.split(",",0);
				 //ファイル内の要素数が2以外、且つ支店コードが半角数字じゃなかった場合のエラー処理
				 if(branch.length != 2 || !branch[0].matches("^[0-9]*$")) {
					 System.out.println("支店定義ファイルのフォーマットが不正です");
				 	 return;
				 }
				 //支店コードが半角数字以外、且つ行数が3桁以外の場合のエラー処理
				 if(!branch[0].matches("^[0-9]{3}")) {
				 	 System.out.println("支店定義ファイルのフォーマットが不正です");
				 	 return;
			 	 }


				 prefs.put(branch[0], branch[1]);
				 profit.put(branch[0], 0L);

			 }

		 }catch(IOException e) {
			 System.out.println("予期せぬエラーが発生しました");
			 return;
		 }

		 finally {
			 if(br != null) {
				 try {
					 br.close();
				 }catch(IOException e) {
					 System.out.println("予期せぬエラーが発生しました");
					 return;
				 }
			 }
		 }

		 //売り上げファイルリスト
		 ArrayList<File> profitfile = new ArrayList<File>();
		 //連番チェック用リスト
		 ArrayList<Integer> eightdigits = new ArrayList<Integer>();

		 //フォルダを開く
		 File dir = new File(args[0]);
		 //フォルダ内のファイルを取得
		 File[] fileList = dir.listFiles();
		 //フォルダ内のファイル数分ループを回す
		 for(File sales: fileList) {
			 //売り上げファイルの選別
			 if(sales.getName().matches("[0-9]{8}\\.rcd") && sales.isFile()) {

				 //売り上げはArrayListに入れる
				 profitfile.add(sales);
				 //substringを使って文字列の一部(.rcd前の8桁部分)を取得する
				 eightdigits.add(Integer.parseInt(sales.getName().substring(1,8)));

			 }
		 }


			 /*for (String key: prefs.keySet()) {
			 System.out.println(key + prefs.get(key));
			 }*/
		  //売り上げファイルの数だけループする
		  for(int i = 1; i < eightdigits.size(); i++) {

			 //売り上げファイル名が連番になってない場合のエラー処理
			 if(!(eightdigits.get(i - 1)+ 1 == eightdigits.get(i))) {
				 System.out.println("売上ファイル名が連番になっていません");
				 return;
			 }
		  }

		  //profitfileというArrayListがループする
		  for(int i = 0; i < profitfile.size(); i++) {
			 try {
				 //profitfileを開き、1行目を取得
				 FileReader freader = new FileReader(profitfile.get(i));
				 //profitfileの1行目を取得したファイルを読み込む
				 breader = new BufferedReader(freader);
				 //keyの変数名がsalesLine
				 String salesLine;

				//売り上げファイルリストを開く
				ArrayList<String> proffile = new ArrayList<String>();
				//BufferedReaderクラスのreadLineメソッドを使って1行ずつ読み込み表示する
				while((salesLine = breader.readLine()) != null) {
					//salesLineにある支店コードをproffileにaddする
					proffile.add(salesLine);
				}

				//proffileの要素数が2じゃない場合のエラー処理
				if(proffile.size() != 2) {
					System.out.println(profitfile.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//支店に該当がなかった場合のエラー処理
				if (!prefs.containsKey(proffile.get(0))) {
					 System.out.println(profitfile.get(i).getName() + "の支店コードが不正です");
					 return;
				}

				if(!proffile.get(1).matches("^[0-9]*$")) {
					 System.out.println("予期せぬエラーが発生しました");
				 	 return;
				}



				//proffileのvalue(変数名：listSales)の型をLongにする
				Long listSales = Long.parseLong(proffile.get(1));
				//profitのvalue(変数名：mapSales)の型にproffileのkeyを入れる
				Long mapSales = profit.get(proffile.get(0));

				//valueが初期化されたprofitというmapにproffileのkeyを1行目に、
				//そしてproffileのvalue/売上額(listSales)をprofitのvalueに加算する
				profit.put(proffile.get(0), listSales + mapSales);

				//合計金額が10桁を超えた場合のエラー処理
				if(listSales + mapSales > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			}catch(IOException e) {
			 System.out.println("予期せぬエラーが発生しました");
			 return;
			}

			finally {
				if(breader != null) {
					try {
						breader.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
			if(!output(args[0], "branch.out", prefs, profit)) {
		  	return;

		    }
		  }
	 }

	 public static boolean output(String dirPath, String fileName, Map <String, String> prefs, Map <String, Long> profit) {



	 //集計結果を出力：文字列を「まとめて」出力するための文字出力ストリームクラスBufferedWriterを使う
		 //BufferedWriterクラスは「OSの改行文字」を出力する機能を持つ
		 BufferedWriter bw = null;

	 	 try {
	 		 //フォルダを開く
	 		 //branch.outが支店別集計を出力するファイルの名前
	 		 File file = new File(dirPath, fileName);
	 		 //文字を書き込むために使うFileWriterクラス(変数名：fw)にfileのデータを入れる
			 FileWriter fw = new FileWriter(file);

			 //fwのデータをbwという変数名を持つBufferedWriterに入れる
			 bw = new BufferedWriter(fw);

			 //ループをする
			 for(String key : profit.keySet()) {
				 //bwに入れるのは、prefsに入ってるkey(支店コード)と
				 //prefsのkeyに紐づいてるvalue(支店名)とprofitのkeyに紐づいてるvalue(合計金額)
				 //カンマで3つの出力したデータを区切る
				 bw.write(key + "," + prefs.get(key) + "," + profit.get(key));
				 //改行する
				 bw.newLine();
			 }


		 }catch(IOException e) {
				 System.out.println("予期せぬエラーが発生しました");
				 return false;
		 }

		 finally {
				 if(bw != null) {
					 try {
						 bw.close();
					 }catch(IOException e) {
						 System.out.println("予期せぬエラーが発生しました");
						 return false;
					 }
				 }
		 }
	 	 return true;
	 }
}
